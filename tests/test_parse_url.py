import pytest

from lb.nightly.db.utils import parse_connection_args


@pytest.mark.parametrize(
    "url,expected",
    [
        (
            "http://user:pass@server/dbname",
            ({"url": "http://server", "user": "user", "auth_token": "pass"}, "dbname"),
        ),
        (
            "http://admin:12345@server/service/some-name",
            (
                {
                    "url": "http://server/service/",
                    "user": "admin",
                    "auth_token": "12345",
                },
                "some-name",
            ),
        ),
        (
            "http://server/service/some-name",
            (
                {
                    "url": "http://server/service/",
                    "user": None,
                    "auth_token": None,
                },
                "some-name",
            ),
        ),
    ],
)
def test_urls(url, expected):
    assert parse_connection_args(url) == expected


@pytest.mark.parametrize(
    "url",
    [
        "http://user@server/dbname",
        "http://user:pass@server/",
        "http://user:pass@server/dbname/",
    ],
)
def test_bad_url(url):
    with pytest.raises(ValueError):
        parse_connection_args(db_url=url)
