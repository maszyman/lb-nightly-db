from lb.nightly.db import connect
from lb.nightly.db.database import Database

from unittest.mock import patch
import pytest


@patch("cloudant.client.CouchDB")
def test_basic(cdb):
    assert isinstance(
        connect(db_url="http://me:123@ice.cream/strawberry"),
        Database,
    )

    print(cdb.mock_calls)
    cdb.assert_called_with(
        user="me",
        auth_token="123",
        url="http://ice.cream",
        connect=True,
        auto_renew=True,
        admin_party=False,
    )
    cdb().__getitem__.assert_called_with("strawberry")


@patch(
    "lb.nightly.configuration",
    **{
        "service_config.return_value": {
            "couchdb": {"url": "https://frodo:friend@some-host.example.org/nightly"}
        }
    },
)
@patch("cloudant.client.CouchDB")
def test_from_config(cdb, _conf):
    assert isinstance(connect(), Database)
    print(cdb.mock_calls)
    cdb.assert_called_with(
        user="frodo",
        auth_token="friend",
        url="https://some-host.example.org",
        connect=True,
        auto_renew=True,
        admin_party=False,
    )
    cdb().__getitem__.assert_called_with("nightly")


@patch("cloudant.client.CouchDB")
def test_unauthenticated(cdb):
    assert isinstance(
        connect(db_url="http://ice.cream/strawberry"),
        Database,
    )

    print(cdb.mock_calls)
    cdb.assert_called_with(
        user=None,
        auth_token=None,
        url="http://ice.cream",
        connect=True,
        auto_renew=True,
        admin_party=True,
    )
    cdb().__getitem__.assert_called_with("strawberry")


def test_unsupported_url():
    with pytest.raises(ValueError):
        connect(db_url="ssh://user:pass@stuff/dbname")


@patch(
    "lb.nightly.configuration",
    **{"service_config.side_effect": FileNotFoundError()},
)
@patch("cloudant.client.CouchDB")
def test_config_file_missing(cdb, _conf):
    with pytest.raises(KeyError):
        connect()
