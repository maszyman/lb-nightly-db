###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__version__ = "0.1.0"


def connect(db_url=None):
    from .utils import parse_connection_args
    from urllib.parse import urlparse
    from .database import Database
    from cloudant.client import CouchDB

    args, db_name = parse_connection_args(db_url)

    if urlparse(args["url"]).scheme not in ("https", "http"):
        raise ValueError(f"URL {args['url']} not supported")

    # Note: the CouchDB client allows unauthenticated access only with
    #       admin_party set to True
    return Database(
        CouchDB(
            **args, connect=True, auto_renew=True, admin_party=(args["user"] is None)
        )[db_name]
    )
